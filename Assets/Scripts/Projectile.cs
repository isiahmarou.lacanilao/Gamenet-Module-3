using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public Action<GameObject> OnHit;
    private GameObject User;
    private Rigidbody rb;
    private void Start()
    {
        
    }

    public void Initialize(GameObject user, float additionalSpeed)
    {
        User = user;
        // transform.SetPositionAndRotation(Position.position, user.transform.rotation);
        rb = GetComponent<Rigidbody>();
        rb.AddForce(transform.forward * (1500 + additionalSpeed));
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject == User) return;
        
        if (other.gameObject.CompareTag("Player"))
        {
            OnHit?.Invoke(other.gameObject);
        }
        Destroy(gameObject);
    }
}
