using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class PlayerSetup : MonoBehaviourPunCallbacks
{
    public Camera camera;

    public GameObject combatGFX;

    public TMPro.TMP_Text name_TXT;
    // Start is called before the first frame update
    void Start()
    {
        this.camera = transform.Find("Camera").GetComponent<Camera>();
        if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("rc"))
        {
            GetComponent<VehicleMovementScript>().enabled = photonView.IsMine;
            GetComponent<LapController>().enabled = photonView.IsMine;
            camera.enabled = photonView.IsMine;

            GetComponent<VehicleCombat>().enabled = false;
            combatGFX.SetActive(false);
        }
        else if(PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("dr"))
        {
            GetComponent<VehicleMovementScript>().enabled = photonView.IsMine;
            GetComponent<LapController>().enabled = photonView.IsMine;
            camera.enabled = photonView.IsMine;
            
            GetComponent<VehicleCombat>().enabled = photonView.IsMine;
            combatGFX.SetActive(true);
            
            DeathRaceManager manager = (DeathRaceManager) DeathRaceManager.instance;
            manager.players.Add(photonView.Owner);
        }
        
        name_TXT.text = photonView.Owner.NickName;
        name_TXT.gameObject.SetActive(!photonView.IsMine);
    }
}
