using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
public class RacingGameManager : MonoBehaviour
{
    public GameObject[] vehiclePrefabs;
    public Transform[] startingPositions;
    public GameObject[] finisherTextUI;

    public static RacingGameManager instance = null;
    public Text timerTXT;

    public List<GameObject> lapTriggers = new List<GameObject>();
    protected void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        
        DontDestroyOnLoad(gameObject);
    }

    protected void Start()
    {
        if (PhotonNetwork.IsConnectedAndReady)
        {
            object playerSelectionNumber;

            if (PhotonNetwork.LocalPlayer.CustomProperties.TryGetValue(Constants.PLAYER_SELECTION_NUMBER,
                    out playerSelectionNumber))
            {
                Debug.Log((int) playerSelectionNumber);

                int actorNum = PhotonNetwork.LocalPlayer.ActorNumber;
                Vector3 pos = startingPositions[actorNum - 1].position;
                PhotonNetwork.Instantiate(vehiclePrefabs[(int) playerSelectionNumber].name, pos, Quaternion.identity);
            }
        }

        foreach (GameObject go in finisherTextUI)
        {
            go.SetActive(false);
        }
    }
    
}
