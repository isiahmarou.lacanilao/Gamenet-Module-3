using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;
public class CountdownManager : MonoBehaviourPunCallbacks
{
    public Text timerTXT;

    public float timeToStartRace = 5.00f;
    // Start is called before the first frame update
    void Start()
    {
        timerTXT = RacingGameManager.instance.timerTXT;
    }

    private void Update()
    {
        if (!PhotonNetwork.IsMasterClient) return;
        timerTXT ??= RacingGameManager.instance.timerTXT;

        if (timeToStartRace > 0)
        {
            timeToStartRace -= Time.deltaTime;
            photonView.RPC("SetTime", RpcTarget.AllBuffered, timeToStartRace);
        }
        else if (timeToStartRace < 0)
        {
            photonView.RPC("StartRace", RpcTarget.AllBuffered);
        }
    }

    [PunRPC]
    public void SetTime(float time)
    {
        timerTXT ??= RacingGameManager.instance.timerTXT;
        timerTXT.text = time > 0 ? time.ToString("F1") : "";
    }

    [PunRPC]
    public void StartRace()
    {
        GetComponent<VehicleMovementScript>().isControlEnable = true;
        GetComponent<VehicleCombat>().enableCombat = true;
        this.enabled = false;
    }
}
