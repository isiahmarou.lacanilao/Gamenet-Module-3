using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using Photon.Realtime;
using ExitGames.Client.Photon;
using UnityEngine.UI;
public class LapController : MonoBehaviourPunCallbacks
{
    public List<GameObject> lapTriggers = new List<GameObject>();

    public enum RaiseEventCode
    {
        WhoFinishedEventCode = 0,
        WhoDiedEventCode = 1,
    }

    private int finishOrder = 0;

    public override void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    public override void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    void OnEvent(EventData photonEvent)
    {
        if (photonEvent.Code == (byte) RaiseEventCode.WhoFinishedEventCode)
        {
            object[] data = (object[]) photonEvent.CustomData;

            string nicknameOfFinishPlayer = (string)data[0];
            finishOrder = (int)data[1];
            int viewID = (int) data[2];
            
            Debug.Log(nicknameOfFinishPlayer + " " + finishOrder);

            GameObject orderUiText = RacingGameManager.instance.finisherTextUI[finishOrder - 1];
            orderUiText.SetActive(true);

            if (viewID == photonView.ViewID)
            {
                orderUiText.GetComponent<Text>().text = finishOrder + " " + nicknameOfFinishPlayer + "(YOU)";
                orderUiText.GetComponent<Text>().color = Color.red;
            }
            else
            {
                orderUiText.GetComponent<Text>().text = finishOrder + " " + nicknameOfFinishPlayer;
            }
        }
    }
    
    
    void Start()
    {
        foreach (GameObject go in RacingGameManager.instance.lapTriggers)
        {
            lapTriggers.Add(go);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (lapTriggers.Contains(other.gameObject))
        {
            int indexOfTrigger = lapTriggers.IndexOf(other.gameObject);
            
            lapTriggers[indexOfTrigger].SetActive(false);
        }

        if (other.gameObject.CompareTag("FinishTrigger"))
        {
            GameFinish();
        }
    }

    public void GameFinish()
    {
        GetComponent<PlayerSetup>().camera.transform.SetParent(null);
        GetComponent<VehicleMovementScript>().enabled = false;

        finishOrder++;
        string nickName = photonView.Owner.NickName;
        int viewID = photonView.ViewID;
        object[] data = new object[] {nickName, finishOrder, viewID};
        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOptions = new SendOptions
        {
            Reliability = false
        };
        PhotonNetwork.RaiseEvent((byte) RaiseEventCode.WhoFinishedEventCode, data, raiseEventOptions, sendOptions);
    }
}
